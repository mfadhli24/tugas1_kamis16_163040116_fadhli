<?php 

/**
 * 
 */
class Aksesoris_model
{

	private $table = 'aksesoris';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllAksesoris()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getAksesorisById($id)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}

	public function delAksesorisById($id)
	{
		$this->db->query("DELETE FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		$this->db->one();
		return $this->getAllAksesoris();
	}

	public function insertAksesoris($nama, $kategori, $harga, $stok)
	{

		$this->db->query("INSERT INTO " . $this->table . " VALUES ('', '$nama', '$kategori', '$harga', '$stok')");
		$this->db->one();
		return $this->getAllAksesoris();
	}

	public function updateAksesoris($id, $nama, $kategori, $harga, $stok)
	{

		$this->db->query("UPDATE " . $this->table . " SET nama = '$nama', kategori = '$kategori', harga = '$harga', stok = '$stok' WHERE id = '$id'");
		$this->db->bind("id", $id);
		return $this->db->execute();
	}

	public function searchAksesoris($search)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id LIKE '%$search%' OR nama LIKE '%$search%' OR kategori LIKE '%$search%' OR harga LIKE '%$search%' OR stok LIKE '%$search%'");
		return $this->db->resultSet();
	}

	function __destruct(){
		}
}