<div class="container mt-5">
	
	<div class="row">
		<div class="col-6">
			<h3>Daftar Aksesoris</h3>
			<ul class="list-group ">
					<li class="d-flex justify-content-end align-items-center">
						<a href="<?= BASEURL; ?>/aksesoris/insert" class="btn btn-success">New Record</a>
					</li>
				</ul>
			<?php foreach ($data['acc'] as $key) : ?>
				<ul class="list-group mt-3">
					<li class="list-group-item d-flex justify-content-between align-items-center">
						<?= $key['nama'] ?>
						<div>
						<a href="<?= BASEURL; ?>/aksesoris/detail/<?= $key['id']; ?>" class="badge badge-primary">Detail</a>
						<a href="<?= BASEURL; ?>/aksesoris/delete/<?= $key['id']; ?>" class="badge badge-danger">Delete</a>
						</div>
					</li>
				</ul>
			<?php endforeach; ?>
		</div>
	</div>
</div>