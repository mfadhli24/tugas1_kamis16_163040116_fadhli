<div class="container mt-5">
	<div class="row">
		<div class="col-6">
		<h3>Tambah Daftar Aksesoris</h3>
			<form action="insert" method="post">
			  <div class="form-group">
			    <label for="formGroupExampleInput">Nama Barang</label>
			    <input type="text" name="nama"  class="form-control">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Kategori</label>
			    <input type="text" name="kategori"  class="form-control">
			  </div>
			  <div class="form-group">
			  <label for="formGroupExampleInput">Harga</label>
			    <input type="numeric" name="harga"  class="form-control">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Stok</label>
			    <input type="numeric" name="stok" class="form-control">
			  </div>
			  <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
			</form>
		</div>
	</div>
</div>