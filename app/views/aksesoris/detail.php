<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><?= $data['acc']['nama'] ?></h5>
			<h6 class="card-subtitle mb-2 text-muted">Kategori : <?= $data['acc']['kategori'] ?></h6>
			<p class="card-text">Harga : <?= $data['acc']['harga'] ?></p>
			<p class="card-text">Stok : <?= $data['acc']['stok'] ?></p>
			<ul class="justify-content-end">
			<a href="<?= BASEURL; ?>/aksesoris" class="card-link">Kembali</a>
			<a href="<?= BASEURL; ?>/aksesoris/update/<?= $data['acc']['id'] ?>" class="card-link">Update</a>
			</ul>
		</div>
	</div>
</div>