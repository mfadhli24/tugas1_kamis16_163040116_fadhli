<div class="container mt-5">
	<div class="card" style="width: 18rem;">
		<div class="card-body">
 			<a href="<?= BASEURL; ?>/aksesoris">
	 			<button type="button" class="close" aria-label="Close">
	 				<span aria-hidden="true">&times;</span>
	 			</button>
 			</a>
			<h4>Tambah Daftar Aksesoris</h4>
			<form action="update" method="post">
			  <div class="form-group">
			    <label for="formGroupExampleInput">Nama Barang</label>
			    <input type="hidden" name="id"  class="form-control" value="<?= $data['acc']['id'] ?>">
			    <input type="text" name="nama"  class="form-control" value="<?= $data['acc']['nama'] ?>">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Kategori</label>
			    <input type="text" name="kategori"  class="form-control" value="<?= $data['acc']['kategori'] ?>">
			  </div>
			  <div class="form-group">
			  <label for="formGroupExampleInput">Harga</label>
			    <input type="numeric" name="harga"  class="form-control" value="<?= $data['acc']['harga'] ?>">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Stok</label>
			    <input type="numeric" name="stok" class="form-control" value="<?= $data['acc']['stok'] ?>">
			  </div>
			  <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
			</form>
		</div>
	</div>
</div>