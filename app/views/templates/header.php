<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Halaman <?= $data['judul'] ?></title>
		<link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar navbar-light" style="background-color: #00c3ff;">
			<div class="container">
			<a class="navbar-brand" href="<?= BASEURL ?>">PHP MVC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="nav-link" href="<?= BASEURL ?>">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= BASEURL ?>/aksesoris">Aksesoris</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= BASEURL ?>/about">About</a>
					</li>
				</ul>
			</div>
			<form class="form-inline" action="<?= BASEURL ?>/aksesoris/search" method="POST">
				<input class="form-control mr-sm-2" type="text" placeholder="Search" name="search">
				<input class="btn btn-outline-success my-2 my-sm-0" type="submit" name="submit" value="Search">
			</form>
		</nav>
		</div>