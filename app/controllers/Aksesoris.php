<?php 

/**
 * 
 */
class Aksesoris extends Controller
{
	public function index()
	{
		$data = array(
			'judul' => "Daftar Aksesoris",
			'acc' => $this->model('Aksesoris_model')->getAllAksesoris()
		);
		$this->view('templates/header', $data);
		$this->view('Aksesoris/index', $data);
		$this->view('templates/footer');
	}

	public function detail($id)
	{
		$data = array(
			'judul' => "Daftar Aksesoris",
			'acc' => $this->model('Aksesoris_model')->getAksesorisById($id)
		);
		$this->view('templates/header', $data);
		$this->view('Aksesoris/detail', $data);
		$this->view('templates/footer');
	}
	
	public function delete($id)
	{
		$data = array(
			'acc' => $this->model('Aksesoris_model')->delAksesorisById($id)
		);
		$this->view('templates/header', $data);
		$this->view('Aksesoris/index', $data);
		$this->view('templates/footer');
	}
	
	public function insert()
	{ 
		if (isset($_POST['submit'])) {
			$nama = $_POST['nama'];
			$kategori = $_POST['kategori'];
			$harga = $_POST['harga'];
			$stok = $_POST['stok'];
			$this->model('Aksesoris_model')->insertAksesoris($nama, $kategori, $harga, $stok);
		}

		$data['judul'] = "Tambah Daftar Aksesoris";
		$this->view('templates/header', $data);
		$this->view('Aksesoris/insert');
		$this->view('templates/footer');
	}

	public function search()
	{

		if (isset($_POST['submit'])) {
			$search = $_POST['search'];
			
			$data = array(
			'acc' => $this->model('Aksesoris_model')->searchAksesoris($search)
			);

			$this->view('templates/header', $data);
			$this->view('Aksesoris/index', $data);
			$this->view('templates/footer');

			} else {

			$this->view('templates/header', $data);
			$this->view('Aksesoris/update', $data);
			$this->view('templates/footer');
			}
		
	}

	public function update($id)
	{
		if (isset($_POST['submit'])) {
			$id = $_POST['id'];
			$nama = $_POST['nama'];
			$kategori = $_POST['kategori'];
			$harga = $_POST['harga'];
			$stok = $_POST['stok'];
			$this->model('Aksesoris_model')->updateAksesoris($id, $nama, $kategori, $harga, $stok);
		}

		$data = array(
			'judul' => "Daftar Aksesoris",
			'acc' => $this->model('Aksesoris_model')->getAksesorisById($id)
		); 

		$this->view('templates/header', $data);
		$this->view('Aksesoris/update', $data);
		$this->view('templates/footer');
	}



}